// console.log("Hello World!");

// cube of a number
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);


// array address
const completeAddress = ["258 Washington Ave", "NW, California", 90011];
const [address, state, zipcode] = completeAddress;
console.log(`I live at ${address} ${state} ${zipcode}`);


// object animal
const animal = {
    name: "Lolong",
    typeOfAnimal: "saltwater crocodile",
    weight: 1075,
    measurementFeet: 20,
    measurementInches: 3
}
const {name, typeOfAnimal, weight, measurementFeet, measurementInches} = animal;
console.log(`${name} was a ${typeOfAnimal}. He weighed at ${weight} kgs with a measurement of ${measurementFeet} ft ${measurementInches} in.`);


// loop numbers through array using forEach
const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => {
    console.log(numbers);
})


// using reduce()
let reducedNumber = numbers.reduce((acc,cur) => {
    return acc + cur;
});
console.log(reducedNumber);


// constructor
class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}
const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);

